# ulco.m2.NDSFA

## Description :

### Non Dominated Solution Filter Algorithm
<br>

Projet étudiant M2 - Optimisation multi objectif

Algorithme C++ permettant de filtrer un ensemble de solutions afin de ne retourner que les solutions non dominées.

## Auteur :

- [Ringot Arthur](https://gitlab.com/aringot1612)

## Aperçu (Affichage complet):

<br>![Aperçu : Console](images/console.png)

## Graphiques :

<br>![nonDom_rho0.0](images/R/nonDom_rho0.0.jpg)
<br>![nonDom](images/R/nonDom.jpg)
<br>![objSpace_2_-0.8_1](images/R/objSpace_2_-0.8_1.jpg)
<br>![objSpace_2_0_1](images/R/objSpace_2_0_1.jpg)
<br>![objSpace_2_0.8_1](images/R/objSpace_2_0.8_1.jpg)

## Exécution (sous linux de préférence) :

<br>

```c++
bash ./run.sh
```