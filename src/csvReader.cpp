#include <string>
#include <fstream>
#include <vector>
#include <utility>
#include <stdexcept>
#include <sstream>
#include <iostream>

class csvReader{
    private:
        std::ofstream myFile;
    public:
        std::vector<std::vector<double>> read_csv(std::string filename){
            std::vector<std::vector<double>>result;
        std::ifstream myFile(filename);
        if(!myFile.is_open()) throw std::runtime_error("Could not open file");
        std::string line, colname;
        double val;
        int rowIdx = 0;
        while(std::getline(myFile, line))
        {
            std::stringstream ss(line);
            result.push_back({});
            while(ss >> val){
                result[rowIdx].push_back(val);
                if(ss.peek() == ',') ss.ignore();
            }
            rowIdx++;
        }
        myFile.close();
        return result;
    }

        void print(std::vector<std::vector<double>> values){
            for(unsigned int i = 0 ; i < values.size() ; i++){
                for(unsigned int j = 0 ; j < values[i].size() ; j++){
                    std::cout << values[i][j] << " ";
                }
                std::cout << std::endl;
            }
        }
};
