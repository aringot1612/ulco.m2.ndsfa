#include <random>
#include "solution.cpp"
#include "csvReader.cpp"

/**
 * @brief Initialise un ensemble de solution.
 * 
 * @param size Le nombre de solution à générer.
 * @param fitnessNb Le nombre de fitness à générer par solution.
 * @return std::vector<Solution> L'ensemble de solution obtenu.
 */
std::vector<Solution> initSolutions(std::string filename){
	csvReader csv;
	std::vector<std::vector<double>> values = csv.read_csv(filename);
	std::vector<Solution> solutions;
	// Pour chaque solution à générer...
	for(unsigned int i = 0 ; i < values.size() ; i++){
		// Ajout d'une nouvelle solution.
		solutions.push_back(Solution(values.at(i), i));
	}
	// On retourne la liste des solutions.
	return solutions;
}

/**
 * @brief Permet de retourner les solutions non dominées parmi un ensemble de solution.
 * 
 * @param solutions L'ensemble de solution à utiliser pour le filtrage.
 * @return std::vector<Solution> L'ensemble de solutions non dominées.
 */
std::vector<Solution> findNonDominatedSolutions(std::vector<Solution> solutions){
	// Vecteur de solution non dominées vide.
	std::vector<Solution> nonDominatedSolutions;
	// Pour chaque solution...
	for(unsigned int i = 0 ; i < solutions.size() ; i++){
		// On utilise un index secondaire à zero pour parcourir les solutions.
		int sIndex = 0;
		// On récupére la première solution : solution temporaire.
		Solution s = solutions[sIndex];
		// On considère la solution courante comme non dominée.
		bool dominated = false;
		// Tant que la solution courante est non dominée et qu'il reste des solutions à comparer...
		while(!dominated && sIndex < solutions.size()){
			// Si la solution courante est bien dominée par la solution temporaire...
			if(solutions[i].isDominatedBy(s))
				dominated = true;
			// Sinon, la solution courante domine la solution temporaire...
			else {
				// On passe à la solution temporaire suivante.
				sIndex++;
				if(sIndex < solutions.size())
					s = solutions[sIndex];
			}
		}
		// Si la solution courante reste dominée jusque là...
		if(!dominated)
			// On l'ajoute à la liste des solutions non dominées.
			nonDominatedSolutions.push_back(solutions[i]);
	}
	// On retourne la liste des solutions non dominées.
	return nonDominatedSolutions;
}

/**
 * @brief Affiche un ensemble de solutions rapidement.
 * 
 * @param solutions L'ensemble de solutions à afficher.
 */
void printSolutions(std::vector<Solution> solutions){
	for(unsigned int i = 0 ; i < solutions.size() ; i++){
		solutions[i].print();
	}
}

/**
 * @brief Affiche le nombre de solution dans un ensemble.
 * 
 * @param solutions L'ensemble de solutions à utiliser.
 */
void printSolutionsCount(std::vector<Solution> solutions){
	std::cout << solutions.size() << std::endl;
}

/**
 * @brief Affiche les paramètres de lancement.
 * 
 */
void printParameters(int solutionsNb, int fitnessNb){
	std::cout << "Paramètres : \n- Nombre de solutions : " << solutionsNb << "\n- Nombre de valeurs de fitness par solution : " << fitnessNb << std::endl;
}

/**
 * @brief Programme principal.
 * 
 * @return int Retour.
 */
int main(int argc, char *argv[]){
	std::string fileName = "";
	if(argc == 2){
		fileName = argv[1];
	}
	// Initialisation des solutions.
	std::vector<Solution> solutionSet = initSolutions(fileName);
	// Filtrage des solutions non dominées.
	std::vector<Solution> nonDominatedSolutions = findNonDominatedSolutions(solutionSet);
	printSolutionsCount(nonDominatedSolutions);
	// Affichage des résultats.
	////std::cout << "\nEnsemble de solutions\n" << std::endl;
	//printSolutions(solutionSet);
	//std::cout << "\nSolutions non dominées | ";
	//printSolutionsCount(nonDominatedSolutions);
	//std::cout << std::endl;
	//printSolutions(nonDominatedSolutions);
	return 1;
};