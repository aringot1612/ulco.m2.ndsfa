#include <iostream>
#include <vector>

class Solution{
	private:
		// Ensemble de fitness.
		std::vector<double> fitness;
		// Identifiant de solution.
		int id;

	public:
		/**
		 * @brief Construction d'une instance de solution.
		 * 
		 * @param fitness l'ensemble des fitness de cette solution.
		 * @param id son identifiant.
		 */
		Solution(std::vector<double> fitness, int id) : fitness(fitness), id(id){}

		/**
		 * @brief Fonction d'affichage d'une solution.
		 * 
		 */
		void print(){
			std::cout << "Solution " << id << " / fitness -> " ;
			for(unsigned int i = 0 ; i < fitness.size() - 1 ; i++){
				std::cout << fitness[i] << " | ";
			}
			std::cout << fitness[fitness.size() - 1];
			std::cout << std::endl;
		}

		/**
		 * @brief Fonction permettant de vérifier si la solution courante est dominée ou non par la solution fournie en paramètre.
		 * 
		 * @param solution Solution à tester
		 * @return true si la solution est bien dominée par la solution en paramètre.
		 * @return false si la solution n'est pas dominée par la solution en paramètre.
		 */
		bool isDominatedBy(Solution solution){
			// Par défaut, la solution à vérifier ne possède pas de meilleur fitness que la solution courante en comparant chaque fitness respectivement.
			bool greaterFitness = false;
			// Pour chaque fitness à vérifier...
			for(unsigned int i = 0 ; i < fitness.size() ; i++)
				// Si la fitness de la solution à comparer est strictement inférieure à celle de la solution courante, la solution à comparer ne sera pas dominante sur la solution courante.
				if(solution.getFitness(i) < getFitness(i))
					// On retourne directement false : la solution en paramètre ne domine pas.
					return false;
				// Sinon, si la fitness est strictement supérieure à celle de la solution courante...
				else if (solution.getFitness(i) > getFitness(i))
					// La solution à comparer possède au moins une fitness dominante.
					greaterFitness = true;
			// On retourne true si la solution en paramètre a dominé la solution courante, false sinon.
			return greaterFitness;
		}

		/**
		 * @brief Retourne une valeur de fitness via son index.
		 * 
		 * @param index Indicateur de la fitness à récupérer.
		 * @return double Fitness obtenue.
		 */
		double getFitness(int index){
			return this->fitness[index];
		}
};