#!/bin/bash

# Author: Sebastien Verel, Univ. du Littoral Côte d'Opale, France
# Date: 2020/10/11

listD="2 3 4 5"
listRho="-0.8 -0.2 0 0.8"
n=30

echo d rho id n > nb_nondomined.csv
for d in $listD
do
	for rho in $listRho
	do
		a=`echo '(-1 / ('$d' - 1)) < '$rho | bc -l`
		if [ "$a" -eq 1 ]
		then
			for((i=1;i<=$n;i++))
			do
				echo -n $d $rho ${i}' ' >> nb_nondomined.csv
				./build/main ./data/exo2_${d}_${rho}_${i}.csv >> nb_nondomined.csv
			done
		fi;
	done
done
