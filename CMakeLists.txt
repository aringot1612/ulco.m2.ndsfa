cmake_minimum_required( VERSION 3.13 )
project(NDSFA)
FILE(GLOB Sources src/*.cpp)
ADD_EXECUTABLE(main ${Sources})